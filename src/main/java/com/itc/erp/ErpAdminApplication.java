package com.itc.erp;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@EnableAdminServer
public class ErpAdminApplication {
	private static final Logger logger = LoggerFactory.getLogger(ErpAdminApplication.class);

	public static void main(String[] args) {

		SpringApplication.run(ErpAdminApplication.class, args);
	}

}

